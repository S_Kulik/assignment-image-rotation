#pragma once

#include "util.h"
#include <stdio.h>

enum bmpReadStatus {
  BRS_ReadOK = 0,
  BRS_InvalidHeader,
  BRS_InvalidFile,
  BRS_OpenFileError
};
enum bmpOUTStatus { BOS_OutOK = 0, BOS_WritingError, BOS_OpenOutFileError };
enum bmpReadStatus fromBmp(FILE *in, struct picture *ans);
enum bmpOUTStatus toBmp(FILE *OUT, struct picture *p);
