#pragma  once

#include "picture.h"
#include <inttypes.h>

uint8_t getPadding(struct picture* pic);
