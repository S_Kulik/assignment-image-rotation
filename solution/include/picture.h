#pragma  once

#include <inttypes.h>

struct pixel {
    uint8_t B;
    uint8_t G;
    uint8_t R;
};

struct picture {
    uint32_t n, m;
    struct pixel *pic;

};

int64_t get_index(uint32_t x, uint32_t y, struct picture* picture);
struct picture newPicture(int n, int m);
