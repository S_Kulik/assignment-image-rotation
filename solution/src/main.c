#include "../include/FormatBMP.h"
#include "../include/transformations.h"
#include <malloc.h>
#include <stdio.h>

static int read (FILE *input, struct picture* pic) {
  switch (fromBmp(input, pic)) {
  case BRS_ReadOK:
    break;
  case BRS_InvalidHeader:
    fprintf(stderr, "Invalid Header format\n");
    fclose(input);
    return 1;
  case BRS_InvalidFile:
    fclose(input);
    fprintf(stderr, "Invalid file error\n");
    return 1;
  case BRS_OpenFileError:
    fprintf(stderr, "Open file error\n");
    return 1;
  }
  return 0;
}

static int write(FILE* OUT, struct picture* answer) {
  switch (toBmp(OUT, answer)) {
  case BOS_OutOK:
    break;
  case BOS_WritingError:
    fprintf(stderr, "error while writing bmp output file\n");
    return 1;
  case BOS_OpenOutFileError:
    fprintf(stderr, "Open Output File Error");
    return 1;
  }
  return 0;
}

static void freeAll(struct picture* answer, struct picture* pic) {
  free(answer->pic);
  free(pic->pic);
}

int main(int argc, char **argv) {
  (void)argc;
  (void)argv; // supress 'unused parameters' warning
  if (argc < 3) {
    fprintf(stderr, "Not enough arguments\n");
    return 1;
  }
  FILE *input = fopen(argv[1], "rb");
  struct picture pic = {0};
  int x = read(input, &pic);
  fclose(input);
  if (x) {
    return 1;
  }
  struct picture answer = rotate90(pic);
  FILE *OUT = fopen(argv[2], "wb");
  x = write(OUT, &answer);
  freeAll(&answer, &pic);
  fclose(OUT);
  if (x) {
    return 1;
  }
  return 0;
}
