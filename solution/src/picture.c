#include "../include/picture.h"
#include <malloc.h>

struct picture newPicture(int n, int m) {
  return ((struct picture){n, m, malloc(sizeof(struct pixel) * n * m)});
}

int64_t get_index(uint32_t x, uint32_t y, struct picture *pic) {
  return x * pic->m + y;
}
