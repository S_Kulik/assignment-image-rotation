#include "../include/util.h"
#include <inttypes.h>

uint8_t getPadding(struct picture* pic) {
    return (4 - (pic->m * 3) % 4) % 4;
}
