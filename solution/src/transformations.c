#include "../include/transformations.h"
#include <malloc.h>

void pictureConstructor(struct picture* ans, uint32_t n, uint32_t m) {
  ans->pic = (struct pixel *)malloc(sizeof(struct pixel) * n * m);
  ans->n = m;
  ans->m = n;
}

struct picture rotate90(struct picture a) {
  struct picture ans;
  pictureConstructor(&ans, a.n, a.m);
  for (int i = 0; i < a.n; i++) {
    for (int j = 0; j < a.m; j++) {
      ans.pic[get_index(j, i, &ans)] = a.pic[get_index(a.n - i - 1, j, &a)];
    }
  }
  return ans;
}
