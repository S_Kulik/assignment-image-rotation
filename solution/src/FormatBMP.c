#include "../include/FormatBMP.h"
#include <malloc.h>

struct __attribute__((packed)) bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

static struct bmp_header makeBmpHeader(struct picture *pic) {
  uint8_t padding = getPadding(pic);
  return (struct bmp_header){ .bfType = 19778,
  .bfileSize = sizeof(struct bmp_header) + pic->n * pic->m * 3 +
                                 padding * pic->n,
                            .bfReserved = 0,
                            .bOffBits = 54,
                            .biSize = 40,
                            .biWidth = pic->m,
                            .biHeight = pic->n,
                            .biPlanes = 1,
                            .biBitCount = 24,
                            .biCompression = 0,
                            .biSizeImage = pic->n * pic->m * 3 + padding * pic->n,
                            .biXPelsPerMeter = 2834,
                            .biYPelsPerMeter = 2834,
                            .biClrUsed = 0,
                            .biClrImportant = 0};
}

static int correct_bmp_header(struct bmp_header header) {
    if (header.bfType != 19778) {
        return -1;
    }
    return 0;
}

enum bmpReadStatus fromBmp(FILE *in, struct picture *ans) {
  if (in == NULL) {
    return BRS_OpenFileError;
  }
  struct bmp_header header;
  size_t r;
  r = fread(&(header), sizeof(header), 1, in);
    if (r < 1) {
        return BRS_InvalidHeader;
    }
    if (correct_bmp_header(header)) {
        return BRS_InvalidHeader;
    }
  uint32_t n = header.biHeight;
  uint32_t m = header.biWidth;
  ans->n = n;
  ans->m = m;
    struct pixel* from_data = (struct pixel *)malloc(sizeof(struct pixel) * n * m);
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      r = fread((&from_data[get_index(i, j, ans)]), sizeof(struct pixel), 1, in);
      if (r < 1) {
        free(from_data);
        return BRS_InvalidFile;
      }
    }
    r = fseek(in, getPadding(ans), SEEK_CUR);
    if (r) {
        free(from_data);
        return BRS_InvalidFile;
    }
  }
  if (ferror(in)) {
    free(from_data);
    return BRS_InvalidFile;
  }
  ans->pic = from_data;
  return BRS_ReadOK;
}

enum bmpOUTStatus toBmp(FILE *OUT, struct picture *p) {
  if (OUT == NULL) {
    return BOS_OpenOutFileError;
  }
  struct bmp_header header = makeBmpHeader(p);
  size_t r;
  r = fwrite(&(header), sizeof(header), 1, OUT);
  if (r < 1) {
    return BOS_WritingError;
  }
  for (int i = 0; i < p->n; i++) {
    for (int j = 0; j < p->m; j++) {
      r = fwrite(&(p->pic[i * p->m + j]), sizeof(p->pic[i * p->m + j]), 1, OUT);
      if (r < 1) {
        return BOS_WritingError;
      }
    }
    r = fwrite(&(p->pic[0]), 1, getPadding(p), OUT);
    if (r < getPadding(p)) {
        return BOS_WritingError;
    }
  }
  return BOS_OutOK;
}
